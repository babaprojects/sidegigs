import React from 'react';
import { FontAwesome } from '@expo/vector-icons';

import { theme } from "../../../constants";

export default function TabBarIconCalendar(props) {
  return (
    <FontAwesome
      name={props.name}
      size={19.5}
      style={{ marginBottom: -3 }}
      color={props.focused ? theme.colors.primary : theme.colors.black}
    />
  );
}
