import React from 'react';
import { FontAwesome } from '@expo/vector-icons';

import { theme } from "../../../constants";

export default function TabBarIcon(props) {
  return (
    <FontAwesome
      name={props.name}
      size={20}
      style={{ marginBottom: -3 }}
      color={props.focused ? theme.colors.primary : theme.colors.black}
    />
  );
}
