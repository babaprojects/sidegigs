import React from 'react';
import { FontAwesome } from '@expo/vector-icons';

import { theme } from "../../../constants";

export default function TabBarIconMail(props) {
  return (
    <FontAwesome
      name={props.name}
      size={18.5}
      style={{ marginBottom: -2 }}
      color={props.focused ? theme.colors.primary : theme.colors.black}
    />
  );
}
