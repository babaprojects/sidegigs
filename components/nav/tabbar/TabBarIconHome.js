import React from 'react';
import { FontAwesome } from '@expo/vector-icons';

import { theme } from "../../../constants";

export default function TabBarIconHome(props) {
  return (
    <FontAwesome
      name={props.name}
      size={23.5}
      style={{ marginBottom: -3 }}
      color={props.focused ? theme.colors.primary : theme.colors.black}
    />
  );
}
