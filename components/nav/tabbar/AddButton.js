import React from 'react';
import { Ionicons } from '@expo/vector-icons';

import { theme } from "../constants";
import

export default function AddButton() {
  return (
    <Ionicons
      name={props.name}
      size={26}
      style={{ marginBottom: -3 }}
      color={props.focused ? theme.colors.tabIconDefault : theme.colors.primary}
    />
  );
}
