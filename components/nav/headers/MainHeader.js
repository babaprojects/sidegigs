import React from 'react';
import { StyleSheet, Image, Dimensions, View, Modal, ScrollView } from 'react-native';
import { FontAwesome as Icon } from '@expo/vector-icons';

import { Button, Block, Text, Card, Divider } from '../../../common';
import { theme } from '../../../constants';

import Dashboard from './Dashboard';

const { width, height } = Dimensions.get('window');

export default class MainHeader extends React.Component {
  state = {
    showTerms: false,
    showAddModal: false
  }

  renderProfile() {
    return (
      <Modal animationType="slide" visible={this.state.showTerms} onRequestClose={() => this.setState({ showTerms: false })}>
        <Block padding={[theme.sizes.padding, 0]}>
          <Icon
            name="close"
            size={25}
            style={styles.closeButton}
            color={theme.colors.gray}
            onPress={() => this.setState({ showTerms: false })}
          />
          <Block>
            <Dashboard navigation={this.props.navigation} />
          </Block>
        </Block>
      </Modal>
    )
  }

  renderRequestGig() {
    return (
      <Modal animationType="slide" visible={this.state.showAddModal} onRequestClose={() => this.setState({ showAddModal: false })}>
        <Block padding={[theme.sizes.padding, 0]}>
          <Icon
            name="close"
            size={25}
            style={styles.closeButton}
            color={theme.colors.gray}
            onPress={() => this.setState({ showAddModal: false })}
          />
          <Block>
            <Text>Request</Text>
          </Block>
        </Block>
      </Modal>
    )
  }

  render() {
    return (
      <Block flex={false}>
        <Block flex={false} style={styles.headerMain} row>
          <View style={{ width: (width - (theme.sizes.base * 3)) * 0.70 }}>
            <View style={{ flexDirection: 'row' }}>
              <Button style={styles.headerCircle} onPress={() => this.setState({ showTerms: true })}>
                <Image
                  source={require('../../../assets/images/avatar.jpg')}
                  style={styles.headerCircle}
                />
              </Button>
              <Block flex={false} style={styles.avatarText}>
                <Text header bold>DJ Fred</Text>
                <Text smallx gray style={styles.location}>TORONTO</Text>
              </Block>
            </View>
          </View>
          <View style={{ width: (width - (theme.sizes.base * 3)) * 0.30 }}>
            <Button shadow style={{...styles.headerCircle, alignSelf: 'flex-end'}} onPress={() => this.setState({ showAddModal: true })}>
              <Icon
                name="bell-o"
                size={16}
                style={{ alignSelf: 'center' }}
                color={theme.colors.primary}
              />
            </Button>
          </View>
        </Block>
        {this.renderProfile()}
        {this.renderRequestGig()}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  headerMain: {
    paddingHorizontal: theme.sizes.base * 1.5,
    paddingTop: theme.sizes.padding,
    paddingBottom: theme.sizes.default,
    backgroundColor: theme.colors.white,

  },
  avatarText: {
    paddingVertical: theme.sizes.default - 2,
    paddingHorizontal: theme.sizes.default
  },
  headerCircle: {
    height: theme.sizes.base * 2.5,
    width: theme.sizes.base * 2.5,
    borderRadius: (theme.sizes.base * 2.5) / 2,
  },
  closeButton: {
    alignSelf: 'flex-end',
    marginRight: theme.sizes.padding * 1.25
  }
});
