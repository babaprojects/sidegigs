import React from 'react';
import { StyleSheet, Image, Dimensions, View, ScrollView } from 'react-native';
import { FontAwesome as Icon } from '@expo/vector-icons';

import { Button, Block, Text, Card, Divider } from '../../../common';
import { theme } from '../../../constants';

const { width, height } = Dimensions.get('window');

export default class Dashboard extends React.Component {
  render() {
    return (
      <Block animated background>
        <Block center style={styles.modal}>
          <Text bold style={{ fontSize: 48 }}>$155.20</Text>
          <Text body gray>Amount Earned</Text>
        </Block>
        <Block>
          <ScrollView horizontal={true} style={styles.explore} showsHorizontalScrollIndicator={false}>
            <Card shadow style={styles.card}>
              <Block row space="between">
                <Block>
                  <Icon
                    name="briefcase"
                    size={25}
                    color={theme.colors.gray2}
                    style={styles.subIcon}
                  />
                  <Text bold gray body>Gigs Completed</Text>
                </Block>
                <Block center>
                  <Text bold right h2>15</Text>
                </Block>
              </Block>
            </Card>
            <Card shadow style={styles.card}>
              <Block row space="between">
                <Block>
                  <Icon
                    name="star"
                    size={25}
                    color={theme.colors.gray2}
                  />
                  <Text bold gray body>Average Rating</Text>
                </Block>
                <Block center>
                  <Text bold h2>4.5</Text>
                </Block>
              </Block>
            </Card>
          </ScrollView>
        </Block>
        <Block>
          <Block row>
            <Divider />
            <Text h3 bold style={{ margin: theme.sizes.base }}>GO TO</Text>
            <Divider />
          </Block>
          <Block row center space="between" style={{ marginTop: 0, paddingHorizontal: theme.sizes.base * 1.6 }}>
            <Button shadow style={styles.headerCircle}>
              <Icon
                name="user"
                size={22}
                style={{ alignSelf: 'center' }}
                color={theme.colors.primary}
              />
              <Text center smallx primary>Profile</Text>
            </Button>
            <Button shadow style={styles.headerCircle}>
              <Icon
                name="money"
                size={22}
                style={{ alignSelf: 'center' }}
                color={theme.colors.primary}
              />
              <Text center smallx primary>Wallet</Text>
            </Button>
            <Button
              shadow
              style={styles.headerCircle}>
              <Icon
                name="cog"
                size={22}
                style={{ alignSelf: 'center' }}
                color={theme.colors.primary}
              />
              <Text center smallx primary>Settings</Text>
            </Button>
          </Block>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  explore: {
    paddingLeft: theme.sizes.base * 1.6,
    paddingTop: theme.sizes.default,
    paddingRight: theme.sizes.base * 2.6,
    marginBottom: theme.sizes.base
  },
  modal: {
    paddingTop: theme.sizes.padding * 2
  },
  card: {
    marginRight: theme.sizes.base,
    height: theme.sizes.padding * 4.5,
    width: width - (theme.sizes.padding * 4.5)
  },
  headerCircle: {
    height: theme.sizes.base * 4.5,
    width: theme.sizes.base * 4.5,
    borderRadius: (theme.sizes.base * 4.5) / 2,
    backgroundColor: theme.colors.background
  },
});
