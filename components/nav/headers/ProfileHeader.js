import React from 'react';
import { StyleSheet, Image, Dimensions, View, Modal, ScrollView } from 'react-native';
import { Ionicons as Icon} from '@expo/vector-icons';

import { Button, Block, Text, Card, Divider } from '../../../common';
import { theme } from '../../../constants';

import Dashboard from './Dashboard';

const { width, height } = Dimensions.get('window');

export default class ProfileHeader extends React.Component {
  render() {
    return (
      <Block flex={false}>
        <Block flex={false} row space="between" style={styles.headerMain}>
          <Button onPress={() => this.props.navigation.goBack()}>
            <Icon
              name="ios-arrow-round-back"
              size={39}
              style={styles.backButton}
              color={theme.colors.primary}
            />
          </Button>
          <Text h3 bold right style={{ marginTop: theme.sizes.base - 3 }}>DJ Jessica</Text>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  headerMain: {
    paddingHorizontal: theme.sizes.base * 1.5,
    paddingTop: theme.sizes.padding,
    paddingBottom: theme.sizes.default,
    backgroundColor: theme.colors.white,

  },
  avatarText: {
    paddingVertical: theme.sizes.default - 2,
    paddingHorizontal: theme.sizes.default
  },
  headerCircle: {
    height: theme.sizes.base * 2.5,
    width: theme.sizes.base * 2.5,
    borderRadius: (theme.sizes.base * 2.5) / 2,
  },
  closeButton: {
    alignSelf: 'flex-end',
    marginRight: theme.sizes.padding * 1.25
  }
});
