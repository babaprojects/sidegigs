import React from 'react';
import { StyleSheet, Image, Dimensions, ScrollView, Platform, Animated } from 'react-native';
import { TabView, SceneMap, TabBar, NavigationState, SceneRendererProps, } from 'react-native-tab-view';

import { Button, Block, Text } from '../../common';
import { theme } from '../../constants';

import Gigs from './Gigs';

const { width, height } = Dimensions.get('window');

export default class Bookings extends React.Component {

  _renderTabBar = props => {
    return (
      <TabBar
        {...props}
        indicatorStyle={{ backgroundColor: theme.colors.primary }}
        style={styles.header}
        labelStyle = {{ fontSize: 12, fontWeight: 'bold', color: theme.colors.gray, }}
      />
    );
  };

  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: 'my_bookings', title: 'Bookings' },
        { key: 'my_hires', title: 'Hires' },
      ],
    }
  }

  _renderScene = SceneMap({
    my_bookings: Gigs,
    my_hires: Gigs,
  });

  render() {
    return (
      <Block flex={false}>
        <TabView
          navigationState={this.state}
          renderScene = {this._renderScene}
          renderTabBar={this._renderTabBar}
          onIndexChange={index => this.setState({ index })}
          initialLayout={{ width: Dimensions.get('window').width - 60 }}
        />
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  explore: {
    paddingHorizontal: theme.sizes.base * 1.2,
    paddingVertical: theme.sizes.base
  },
  header: {
    marginHorizontal: theme.sizes.base * 2.2,
    marginBottom: theme.sizes.base * 2,
    height: theme.sizes.base * 3,
    backgroundColor: theme.colors.white,

  }
});
