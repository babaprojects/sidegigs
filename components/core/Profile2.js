import React from 'react';
import { StyleSheet, Image, Dimensions, View, ScrollView } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import { FontAwesome as Stars } from '@expo/vector-icons';

import { Button, Block, Text, Card, Divider } from '../../common';
import { theme } from '../../constants';

const { width, height } = Dimensions.get('window');

export default class Profile extends React.Component {
  render() {
    return (
      <Block animated background>
        <ScrollView showsVerticalScrollIndicator={false} style={styles.explore}>
          <Block flex={false} row center space="between" style={styles.avatarColumn}>
            <Block center>
              <Image
                source={require('../../assets/images/avatar.jpg')}
                style={styles.avatar}
              />
            </Block>
            <Block>
              <Text h4 bold>DJ Jessica</Text>
              <Text gray uppercase>Toronto</Text>
              <Block style={styles.ratingBar} row>
                <Icon
                  name="star"
                  size={16}
                  style={{ marginRight: theme.sizes.small }}
                  color={theme.colors.white}
                />
                <Text white center>4.1</Text>
              </Block>
            </Block>
          </Block>
          <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          <Text gray center>I am an accomplished international DJ with multiple national awards and honors. My sets are mostly punk music blended with afrobeats.</Text>
          <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          <Block>
            <Card shadow>
              <Text h4 center bold>Porfolio - Sample Mix</Text>
              <Divider margin={[theme.sizes.base, theme.sizes.base * 7]} />
              <Block flex={false} row style={styles.cardedBlock}>
                <View style={{ width: (width - (theme.sizes.base * 3.2)) * 0.18 }}>
                  <Icon
                    name="music"
                    size={30}
                    style={styles.playIcon}
                    color={theme.colors.gray}
                  />
                </View>
                <View style={{ width: (width - (theme.sizes.base * 3.2)) * 0.58, marginTop: theme.sizes.default }}>
                  <Text title gray semibold>Live at Radio City Set</Text>
                  <Text smallx black bold>53:30</Text>
                </View>
                <View style={{ width: (width - (theme.sizes.base * 3.2)) * 0.24 }}>
                  <Button style={styles.playButton}>
                    <Text body primary>Play</Text>
                  </Button>
                </View>
              </Block>
            </Card>
          </Block>
          <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          <Block flex={false} row center space="between">
            <Block center middle>
              <Text bold>Fee</Text>
              <Text title primary semibold style={{ marginTop: theme.sizes.font }}>$140/hr</Text>
            </Block>
            <Block center middle>
              <Text bold>Availability</Text>
              <Icon
                name="calendar"
                size={29}
                style={{ marginTop: theme.sizes.padding / 3 }}
                color={theme.colors.gray2}
              />
            </Block>
          </Block>
          <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          <Block>
            <Card shadow>
              <Text h4 center bold>Reviews</Text>
              <Divider margin={[theme.sizes.base, theme.sizes.base * 7]} />
              <Block flex={false} row style={styles.reviews}>
                <View style={{ width: (width - (theme.sizes.base * 3.2)) * 0.20 }}>
                  <Image
                    source={require('../../assets/images/avatar3.jpg')}
                    style={styles.reviewAvatar}
                  />
                </View>
                <View style={styles.quote}>
                  <Text body gray>We loved DJ Jessica's set, it was really inspiring and fun.</Text>
                  <Block flex={false} row>
                    <Stars
                      name="star"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                    <Stars
                      name="star"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                    <Stars
                      name="star"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                    <Stars
                      name="star"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                    <Stars
                      name="star"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                  </Block>
                </View>
              </Block>
              <Block flex={false} row style={styles.reviews}>
                <View style={{ width: (width - (theme.sizes.base * 3.2)) * 0.20 }}>
                  <Image
                    source={require('../../assets/images/avatar4.jpg')}
                    style={styles.reviewAvatar}
                  />
                </View>
                <View style={styles.quote}>
                  <Text body gray>Jess was very pleasant to deal with.</Text>
                  <Block flex={false} row>
                    <Stars
                      name="star"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                    <Stars
                      name="star"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                    <Stars
                      name="star"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                    <Stars
                      name="star"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                    <Stars
                      name="star"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                  </Block>
                </View>
              </Block>
              <Block flex={false} row style={styles.reviews}>
                <View style={{ width: (width - (theme.sizes.base * 3.2)) * 0.20 }}>
                  <Image
                    source={require('../../assets/images/avatar2.jpg')}
                    style={styles.reviewAvatar}
                  />
                </View>
                <View style={styles.quote}>
                  <Text body gray>She was late to the set. Otherwise great performance.</Text>
                  <Block flex={false} row>
                    <Stars
                      name="star"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                    <Stars
                      name="star"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                    <Stars
                      name="star"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                    <Stars
                      name="star"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                    <Stars
                      name="star-o"
                      size={12}
                      style={{ marginTop: theme.sizes.padding / 3 }}
                      color={theme.colors.primary}
                    />
                  </Block>
                </View>
              </Block>
            </Card>
            <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
            <Button gradient onPress = {() => this.props.navigation.navigate('Booking')}>
              <Text center semibold white>Book</Text>
            </Button>
          </Block>
        </ScrollView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  explore: {
    paddingHorizontal: theme.sizes.base * 1.6
  },
  avatarColumn: {
    marginTop: theme.sizes.base * 2.5,
    marginBottom: theme.sizes.base
  },
  avatar: {
    height: theme.sizes.base * 6.0,
    width: theme.sizes.base * 6.0,
    borderRadius: (theme.sizes.base * 6.0) / 2
  },
  reviewAvatar: {
    height: theme.sizes.base * 3.5,
    width: theme.sizes.base * 3.5,
    borderRadius: (theme.sizes.base * 3.5) / 2
  },
  ratingBar: {
    borderRadius: theme.sizes.radius,
    backgroundColor: theme.colors.primary,
    paddingHorizontal: theme.sizes.default,
    paddingVertical: theme.sizes.small,
    width: theme.sizes.base * 3.5
  },
  cardedBlock: {
    marginTop: theme.sizes.default
  },
  reviews: {
    marginVertical: theme.sizes.base
  },
  playIcon: {
    marginLeft: theme.sizes.base,
    marginTop: theme.sizes.base
  },
  playButton: {
    width: theme.sizes.base * 2.5,
    marginLeft: theme.sizes.default
  },
  quote: {
    width: (width - (theme.sizes.base * 3.2)) * 0.80,
    paddingRight: theme.sizes.padding,
    paddingLeft: theme.sizes.small
  }
});
