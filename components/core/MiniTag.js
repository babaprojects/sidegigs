import React from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';

import { Card, Button, Block, Text, Divider } from '../../common';
import { theme } from '../../constants';

export default class MiniTag extends React.Component {
  render() {
    return (
        <Card shadow style={styles.tag}>
          <Text center caption>{this.props.tag}</Text>
        </Card>
    );
  }
}

const styles = StyleSheet.create({
  tag: {
    margin: theme.sizes.default,
    minWidth: theme.sizes.padding * 2.55,
    height: theme.sizes.padding * 1.15,
    padding: theme.sizes.small + 2,
    backgroundColor: theme.colors.background
  }
});
