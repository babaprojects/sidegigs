import React from 'react';
import { StyleSheet, Image, Dimensions, View, Modal, ScrollView, TouchableOpacity } from 'react-native';
import { FontAwesome as Icon } from '@expo/vector-icons';

import { Card, Button, Block, Text, Divider } from '../../common';
import { theme } from '../../constants';

import Profile from './Profile';

const { width, height } = Dimensions.get('window');

export default class GigsterCard extends React.Component {
  renderRatings = (rating, id) => {
    const stars = new Array(rating).fill(0);
    return (
      stars.map((_, id) => {
        return (
          <Icon
            key={id}
            name="star"
            size={12}
            stars
            color={theme.colors.primary}
          />
        )
      })
    )
  }

  state = {
    showTerms: false,
  }

  renderProfile() {
    return (
      <Modal animationType="slide" visible={this.state.showTerms} onRequestClose={() => this.setState({ showTerms: false })}>
        <Block style={{ paddingTop: theme.sizes.padding }}>
          <Icon
            name="close"
            size={19}
            style={styles.closeButton}
            color={theme.colors.gray}
            onPress={() => this.setState({ showTerms: false })}
          />
          <Profile navigation={this.props.navigation} />
        </Block>
      </Modal>
    )
  }

  render() {
    const { navigation } = this.props;
    return (
      <TouchableOpacity activeOpacity={0.75} onPress = {() => navigation.navigate('Profile')}>
        <Card shadow>
          <Block flex={false} row style={{ marginBottom: theme.sizes.base }}>
            <View style={{ width: (width - (theme.sizes.base * 2.4)) * 0.20 }}>
              <Image
                source={this.props.avatar}
                style={styles.gigsterAvatar}
              />
            </View>
            <View style={styles.info}>
              <Text header bold>{this.props.name}</Text>
              <Text smallx style={styles.location}>{this.props.location}</Text>
            </View>
            <View style={styles.view}>
              <Block center row style={styles.button}>
                {this.renderRatings(this.props.rating, this.props.id)}
              </Block>
            </View>
          </Block>
          <Divider style={{ marginVertical: 0, marginHorizontal: theme.sizes.padding * 3 }} />
          <Block row center space="between" style={{ marginVertical: theme.sizes.default, marginLeft: (width - (theme.sizes.base * 2.4)) * 0.05 }}>
            <Block center>
              <View style={{ flexDirection: 'row' }}>
                <Icon
                  name="map-marker"
                  size={14}
                  color={theme.colors.gray2}
                  style={styles.subIcon}
                />
                <Text body semibold gray2>{this.props.distance}</Text>
              </View>
            </Block>
            <Block center>
              <View style={{ flexDirection: 'row' }}>
                <Icon
                  name="briefcase"
                  size={14}
                  color={theme.colors.gray2}
                  style={styles.subIcon}
                />
                <Text body semibold gray2>{this.props.gigs} gigs</Text>
              </View>
            </Block>
            <Block center>
              <View style={{ flexDirection: 'row' }}>
                <Icon
                  name="dollar"
                  size={14}
                  color={theme.colors.gray2}
                  style={styles.subIcon}
                />
                <Text body semibold gray2>{this.props.rate} / hr</Text>
              </View>
            </Block>
            {this.renderProfile()}
          </Block>
          <Divider style={{ marginTop: 0, marginBottom: theme.sizes.default, marginHorizontal: theme.sizes.padding * 3 }} />
        </Card>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  info: {
    width: (width - (theme.sizes.base * 2.4)) * 0.50,
    paddingTop: theme.sizes.default
  },
  view: {
    width: (width - (theme.sizes.base * 2.4)) * 0.30,
    alignSelf: 'center'
  },
  gigsterAvatar: {
    height: theme.sizes.base * 3.2,
    width: theme.sizes.base * 3.2,
    marginTop: theme.sizes.small,
    borderRadius: (theme.sizes.base * 3.2) / 2
  },
  stars: {
    marginTop: theme.sizes.padding / 3.5
  },
  location: {
    color: theme.colors.gray
  },
  subIcon: {
    marginTop: theme.sizes.small,
    marginRight: theme.sizes.small
  },
  closeButton: {
    alignSelf: 'flex-end',
    marginVertical: theme.sizes.default,
    marginRight: theme.sizes.padding * 1.25
  }
});
