import React from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import { FontAwesome as Icon } from '@expo/vector-icons';

import { Card, Button, Block, Text, Divider } from '../../common';
import { theme } from '../../constants';

const { width, height } = Dimensions.get('window');

export default class Gigs extends React.Component {
  render() {
    return (
      <Block>
        <Card shadow style={styles.category}>
          <Block flex={false} row center space="between">
            <Text body gray semibold>19 July 2019</Text>
            <View style={styles.status}>
              <Text caption white>Deposit</Text>
            </View>
          </Block>
          <Text header black bold style={styles.info}>Johnson's 45th Birthday</Text>
          <Block flex={false} row>
            <View style={{ width: (width - (theme.sizes.base * 2.4)) * 0.60 }}>
              <Block row style={styles.detail}>
                <Icon
                  name="user-o"
                  size={16}
                  color={theme.colors.gray2}
                  style={{ marginRight: theme.sizes.small }}
                />
                <Text body gray2>Mark Anthony</Text>
              </Block>
              <Block row style={styles.detail}>
                <Icon
                  name="map-marker"
                  size={16}
                  color={theme.colors.gray2}
                  style={{ marginRight: theme.sizes.small + 2 }}
                />
                <Text body gray2>55 Mark Avenue, Queens NY</Text>
              </Block>
              <Block row style={styles.detail}>
                <Icon
                  name="clock-o"
                  size={16}
                  color={theme.colors.gray2}
                  style={{ marginRight: theme.sizes.small }}
                />
                <Text body gray2>9.00 - 11.45</Text>
              </Block>
            </View>
            <View style={{ width: (width - (theme.sizes.base * 2.4)) * 0.40 }}>
              <Block center>
                <Button gray2 shadow style={styles.button}>
                  <Icon
                    name="check"
                    size={18}
                    style={{ alignSelf: 'center' }}
                    color={theme.colors.gray}
                  />
                </Button>
                <Button gray2 shadow style={styles.button}>
                  <Icon
                    name="close"
                    size={18}
                    style={{ alignSelf: 'center' }}
                    color={theme.colors.gray}
                  />
                </Button>
              </Block>
            </View>

          </Block>
        </Card>


      </Block>
    );
  }
}

const styles = StyleSheet.create({
  status: {
    backgroundColor: theme.colors.primary,
    alignSelf: 'flex-end',
    height: theme.sizes.padding,
    padding: theme.sizes.small,
    borderRadius: theme.sizes.radius / 2
  },
  info: {
    marginTop: theme.sizes.base * 2
  },
  detail: {
    marginTop: theme.sizes.default
  },
  button: {
    height: theme.sizes.base * 2.5,
    width: theme.sizes.base * 2.5,
    borderRadius: (theme.sizes.base * 2.5) / 2,
    marginVertical: theme.sizes.small
  }
});
