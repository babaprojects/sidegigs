import React from 'react';
import { StyleSheet, View, Dimensions, ScrollView, Platform, Animated } from 'react-native';
import { Modalize } from 'react-native-modalize';
import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";


import { Card, Button, Block, Text, Input } from '../../common';
import { theme } from '../../constants';

const { width, height } = Dimensions.get('window');

export default class Wallet extends React.Component {
  modal = React.createRef();

  state = {
    type: ''
  }

  openModal = () => {
    if (this.modal.current) {
      this.modal.current.open();
    }
  };

  closeModal = () => {
    if (this.modal.current) {
      this.modal.current.close();
    }
  };



  renderModal() {
    return (
      <Modalize
        ref={this.modal}
        modalHeight={height * 0.8}
        modalStyle={{ backgroundColor: theme.colors.white, borderColor: theme.colors.primary }}
        openAnimationConfig={{
          timing: { duration: 400 },
          spring: { speed: 20, bounciness: 10 },
        }}
        closeAnimationConfig={{
          timing: { duration: 400 },
          spring: { speed: 20, bounciness: 10 },
        }}
        withReactModal
      >
        <Block margin={[theme.sizes.padding * 1.5, theme.sizes.padding * 1.5]}>
          <CreditCardInput onChange={this._onChange} allowScroll={true} />
          <Button gradient>
            <Text center semibold white>Save Card Information</Text>
          </Button>
        </Block>
      </Modalize>
    );
  }

  render() {
    return (
      <Block flex={false}>
        <Card shadow center style={styles.balanceCard} flex={false}>
          <Text bold primary style={{ fontSize: 48, marginBottom: 0 }}>$155.20</Text>
          <Text bold body gray transform="uppercase" style={{ marginTop: -10 }}>Current Balance</Text>
        </Card>
        <Block flex={false} row space="between" style={{ marginVertical: theme.sizes.default }}>
          <Button gradient style={styles.buttonA} onPress={() => this.setState({ showSupport: false })}>
            <Text center semibold white>Cash Out</Text>
          </Button>
          <Button color="black" style={styles.buttonA} onPress = {() => this.openModal()}>
            <Text center semibold white>Change Payment Info</Text>
          </Button>
        </Block>
        <Card shadow style={styles.transactionsCard} flex={false}>
          <Text bold body gray transform="uppercase" style={{ marginTop: -10, marginBottom: theme.sizes.default }}>TRANSACTIONS</Text>
          <ScrollView contentContainerStyle={{ paddingBottom: theme.sizes.padding }}>
            <Block flex={false} style={styles.transactionItem}>
              <Text caption right gray transform="uppercase">Jan 21</Text>
              <Block row style={{ marginVertical: theme.sizes.small + 3 }}>
              <Text body bold style>Babajide Ajayi</Text>
              <Text body gray style={{ marginTop: 2.5, marginLeft: theme.sizes.small }}>paid a deposit of $100.40 to you tomorrow at the day</Text>
              </Block>
            </Block>

          </ScrollView>
        </Card>
        {this.renderModal()}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  buttonA: {
    width: (width / 2) - (12 * 2),
  },
  balanceCard: {
    height: height / 5,
    marginBottom: 0
  },
  transactionsCard: {
    height: height / 2.15
  },
  transactionItem: {
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.gray3,
    paddingVertical: theme.sizes.default
  }
});
