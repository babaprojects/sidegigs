import React from 'react';
import { StyleSheet, View, Dimensions, ScrollView, Platform, Animated } from 'react-native';

import { Card, Button, Block, Text, Divider } from '../../common';
import { theme } from '../../constants';

import Gig from './Gig';

export default class Gigs extends React.Component {
  render() {
    return (
      <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.explore}>
        <Gig />
        <Gig />
        <Gig />
        <Gig />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  explore: {
    paddingHorizontal: theme.sizes.base * 1.2,
    paddingVertical: theme.sizes.base,
  }
});
