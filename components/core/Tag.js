import React from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';

import { Card, Button, Block, Text, Divider } from '../../common';
import { theme } from '../../constants';

export default class Tag extends React.Component {
  render() {
    return (
        <Card shadow style={styles.tag}>
          <Text center gray bold caption style={{ marginTop: 0 }}>{this.props.tag}</Text>
        </Card>
    );
  }
}

const styles = StyleSheet.create({
  tag: {
    margin: theme.sizes.default,
    minWidth: theme.sizes.padding * 2.55,
    height: theme.sizes.padding * 1.95,
    padding: theme.sizes.base
  }
});
