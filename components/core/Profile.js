import React from 'react';
import { StyleSheet, Image, Dimensions, View, ScrollView } from 'react-native';
import { FontAwesome as Icon } from '@expo/vector-icons';
import { Modalize } from 'react-native-modalize';

import RNPickerSelect from 'react-native-picker-select';
import DatePicker from 'react-native-datepicker';

import { Button, Block, Text, Card, Divider, Input } from '../../common';
import { theme } from '../../constants';

import MiniTag from './MiniTag';

const { width, height } = Dimensions.get('window');

export default class Profile extends React.Component {
  modal = React.createRef();

  openModal = () => {
    if (this.modal.current) {
      this.modal.current.open();
    }
  };

  closeModal = () => {
    if (this.modal.current) {
      this.modal.current.close();
    }
  };

  constructor(props){
    super(props)
    this.state = {date:"2020-01-23"}
  }

  renderModal() {
    return (
      <Modalize
        ref={this.modal}
        modalHeight={height * 0.82}
        modalStyle={{ backgroundColor: theme.colors.white, marginBottom: -20, borderColor: theme.colors.primary }}
        openAnimationConfig={{
          timing: { duration: 400 },
          spring: { speed: 20, bounciness: 10 },
        }}
        closeAnimationConfig={{
          timing: { duration: 400 },
          spring: { speed: 20, bounciness: 10 },
        }}
        withReactModal
      >
        <Block margin={[theme.sizes.default, theme.sizes.padding * 1.5]}>
          <Text semibold center title style={{ marginTop: theme.sizes.base + 2 }}>Briefly describe your gig?</Text>
          <Input
            placeholder="Enter Message"
            multiline={true}
            numberOfLines={10}
            placeholderTextColor={theme.colors.gray2}
            style={[styles.input, styles.description]}
            onChangeText={text => this.setState({ email: text })}
          />
          <Text semibold center title style={{ marginBottom: theme.sizes.base + 2 }}>When is your gig happening?</Text>
          <DatePicker
            style={styles.datepicker}
            date={this.state.date}
            showIcon={false}
            mode="date"
            placeholder="select date"
            format="YYYY-MM-DD"
            minDate="2020-01-01"
            maxDate="2096-06-01"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateInput: {
                paddingVertical: theme.sizes.small,
                height: theme.sizes.base * 3,
                borderRadius: theme.sizes.default,
                borderColor: theme.colors.gray3,
                borderWidth: 2.5,
                color: theme.colors.black,
              },
              placeholderText: {
                color: theme.colors.gray2
              }
            }}
            onDateChange={(date) => {this.setState({date: date})}}
          />
          <Text semibold center title style={{ marginBottom: theme.sizes.base + 2 }}>How long will your gig take?</Text>
          <RNPickerSelect
            onValueChange={value => this.setState({ itemtype: value })}
            placeholder={{ label: '# of hours' }}
            place
            items={[
              { label: '1', value: 1 },
              { label: '2', value: 2 },
              { label: '3', value: 3 },
              { label: '4', value: 4 },
            ]}
            style={pickerSelectStyles}
            Icon={() => {
              return <Icon name="angle-down" size={20} color={theme.colors.gray2} style={{ paddingVertical: theme.sizes.base - 4, paddingRight: theme.sizes.default }} />;
            }}
          />
          <Button gradient onPress = {() => this.openModal()}>
            <Text center semibold white>Book @ $34/hr</Text>
          </Button>
        </Block>
      </Modalize>
    );
  }

  render() {
    return (
      <Block flex={false}>
        {this.renderModal()}
        <ScrollView horizontal={true} contentContainerStyle={styles.explore} showsHorizontalScrollIndicator={false}>
          <Image
            source={require('../../assets/images/avatar.jpg')}
            style={styles.mainImage}
          />
          <Image
            source={require('../../assets/images/dj1.jpg')}
            style={styles.mainImage}
          />
        </ScrollView>
        <Block flex={false} style={{ paddingHorizontal: theme.sizes.base * 1.2, paddingTop: theme.sizes.padding }}>
          <Block flex={false} row space="between">
            <Block row>
              <Icon
                name="map-marker"
                size={14}
                color={theme.colors.gray2}
                style= {styles.subIcon}
              />
              <Text body gray>Missisauga</Text>
            </Block>
            <Block row>
              <Icon
                name="calendar"
                size={13}
                color={theme.colors.gray2}
                style= {{...styles.subIcon, marginTop: 1 }}
              />
              <Text body gray>December 21</Text>
            </Block>
            <Block row style={{ paddingLeft: theme.sizes.base }}>
              <Icon
                name="clock-o"
                size={16}
                color={theme.colors.gray2}
                style= {styles.subIcon}
              />
              <Text body gray>11pm - 2am</Text>
            </Block>
          </Block>
          <Block flex={false} style={styles.tags}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <MiniTag tag="Hip Hop" />
              <MiniTag tag="Afrobeats" />
              <MiniTag tag="EDM" />
              <MiniTag tag="Top 40" />
              <MiniTag tag="Disco" />
            </ScrollView>
          </Block>
          <Block flex={false} row space="between" style={{ marginTop: theme.sizes.default }}>
            <Card shadow style={{ marginRight: theme.sizes.small + 3 }}>
              <Text h2 bold>4.3</Text>
              <Block row>
                <Icon
                  name="star"
                  size={12}
                  color={theme.colors.primary}
                />
                <Icon
                  key={this.props.id}
                  name="star"
                  size={12}
                  stars
                  color={theme.colors.primary}
                />
                <Icon
                  key={this.props.id}
                  name="star"
                  size={12}
                  stars
                  color={theme.colors.primary}
                />
                <Icon
                  key={this.props.id}
                  name="star"
                  size={12}
                  stars
                  color={theme.colors.primary}
                />
              </Block>
            </Card>
            <Card shadow style={{ marginLeft: theme.sizes.small + 3 }}>
              <Block flex={false} row>
              <Icon
                name="music"
                size={24}
                style={{ marginRight: theme.sizes.default * 1.3, marginTop: theme.sizes.small, marginBottom: theme.sizes.default }}
                color={theme.colors.gray2}
              />
              <Icon
                name="image"
                size={24}
                style={{ marginTop: theme.sizes.small, marginBottom: theme.sizes.default }}
                color={theme.colors.gray2}
              />
              </Block>
              <Text body black bold>Samples</Text>
            </Card>
          </Block>
          <Block flex={false} row space="between">
            <Button gradient style={{ width: width * 0.53 }} onPress = {() => this.openModal()}>
              <Text center semibold white>Book @ $34/hr</Text>
            </Button>
            <Block flex={false} row space="between">
              <Button shadow style={styles.headerCircle}>
                <Icon
                  name="heart"
                  size={12}
                  style={{ alignSelf: 'center' }}
                  color={theme.colors.primary}
                />
              </Button>
              <Button shadow style={styles.headerCircle}>
                <Icon
                  name="envelope"
                  size={12}
                  style={{ alignSelf: 'center' }}
                  color={theme.colors.primary}
                />
              </Button>
              <Button shadow style={styles.headerCircle}>
                <Icon
                  name="share"
                  size={12}
                  style={{ alignSelf: 'center' }}
                  color={theme.colors.primary}
                />
              </Button>
            </Block>
          </Block>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  explore: {
    paddingHorizontal: theme.sizes.base * 1.2
  },
  mainImage: {
    width: (width - (theme.sizes.base * 3.5)),
    height: height / 3.45,
    resizeMode: 'cover',
    marginRight: theme.sizes.default,
    borderRadius: theme.sizes.radius,
    shadowColor: theme.colors.black,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 13
  },
  headerCircle: {
    height: theme.sizes.base * 2.15,
    width: theme.sizes.base * 2.15,
    borderRadius: (theme.sizes.base * 2.15) / 2,
    marginHorizontal: theme.sizes.small,
    marginTop: theme.sizes.base
  },
  subIcon: {
    marginRight: theme.sizes.small
  },
  tags: {
    marginTop: theme.sizes.base,
    marginRight: 0
  },
  overlay: {
    backgroundColor: 'rgba(41, 36, 107, 0.9)',
  },
  input: {
    borderRadius: theme.sizes.default,
    borderColor: theme.colors.gray3,
    borderWidth: 2.5,
    paddingTop: theme.sizes.base,
    paddingHorizontal: theme.sizes.default,
    marginBottom: 0
  },
  description: {
    height: height / 5
  },
  datepicker: {
    width: width - (theme.sizes.padding * 1.5 * 2),
    height: theme.sizes.base * 4,
  }
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: theme.sizes.font,
    fontWeight: '500',
    fontFamily: "nunito-regular",
    paddingHorizontal: theme.sizes.default,
    height: theme.sizes.base * 3,
    borderRadius: theme.sizes.default,
    borderColor: theme.colors.gray3,
    borderWidth: 2.5,
    color: theme.colors.black,
    marginBottom: theme.sizes.base
  },
  inputAndroid: {
    fontSize: theme.sizes.font,
    fontWeight: '500',
    fontFamily: "nunito-regular",
    paddingHorizontal: theme.sizes.default,
    height: theme.sizes.base * 3,
    borderRadius: theme.sizes.default,
    borderColor: theme.colors.gray3,
    borderWidth: 2.5,
    color: theme.colors.black,
    marginBottom: theme.sizes.base
  },
});
