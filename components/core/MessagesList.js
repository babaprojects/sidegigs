import React from 'react';
import { StyleSheet, Image, Dimensions, View, Platform, ScrollView, Animated, TouchableOpacity } from 'react-native';
import { FontAwesome as Icon } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

import { Button, Block, Text, Divider, Input } from '../../common';
import { theme } from '../../constants';


const { width, height } = Dimensions.get('window');

export default class MessagesList extends React.Component {

  state = {
    searchFocus: new Animated.Value(0.6),
    searchString: null,
  }

  handleSearchFocus(status) {
    Animated.timing(
      this.state.searchFocus,
      {
        toValue: status ? 1.8 : 0.6, // status === true, increase flex size
        duration: 250, // ms
      }
    ).start();
  }

  renderSearch() {
    const { searchString, searchFocus } = this.state;
    const isEditing = searchFocus && searchString;

    return (
      <Block animated middle flex={searchFocus} style={styles.search}>
        <Input
          placeholder="Search Messages"
          placeholderTextColor={theme.colors.gray2}
          style={styles.searchInput}
          onFocus={() => this.handleSearchFocus(true)}
          onBlur={() => this.handleSearchFocus(false)}
          onChangeText={text => this.setState({ searchString: text })}
          value={searchString}
          onRightPress={() => isEditing ? this.setState({ searchString: null }) : null}
          rightStyle={styles.searchRight}
          rightLabel={
            <Icon
              name={isEditing ? "close" : "search"}
              size={theme.sizes.base / 1.2}
              color={theme.colors.gray2}
              style={styles.searchIcon}
            />
          }
        />
      </Block>
    )
  }

  render() {
    return (
      <Block flex={false}>
        <Block flex={false} row center space="between" style={styles.header}>
          <Text body bold>1 new!</Text>
          {this.renderSearch()}
        </Block>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.explore}>
          <Block flex={false}>
            <Block flex={false} row style={styles.reviews}>
              <View style={{ width: (width - (theme.sizes.base * 2.4)) * 0.20 }}>
                <Image
                  source={require('../../assets/images/avatar3.jpg')}
                  style={styles.reviewAvatar}
                />
              </View>
              <View style={styles.quote}>
                <Text caption bold right>10.00pm</Text>
                <Text body gray>We loved your set, when are you available again.</Text>
              </View>
            </Block>
            <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          </Block>

          <Block flex={false}>
            <Block flex={false} row style={styles.reviews}>
              <View style={{ width: (width - (theme.sizes.base * 2.4)) * 0.20 }}>
                <Image
                  source={require('../../assets/images/avatar4.jpg')}
                  style={styles.reviewAvatar}
                />
              </View>
              <View style={styles.quote}>
                <Text caption bold right>10.00pm</Text>
                <Text body gray>We loved your set, when are you available again.</Text>
              </View>
            </Block>
            <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          </Block>

          <Block flex={false}>
            <Block flex={false} row style={styles.reviews}>
              <View style={{ width: (width - (theme.sizes.base * 2.4)) * 0.20 }}>
                <Image
                  source={require('../../assets/images/avatar2.jpg')}
                  style={styles.reviewAvatar}
                />
              </View>
              <View style={styles.quote}>
                <Text caption bold right>10.00pm</Text>
                <Text body gray>We loved your set, when are you available again.</Text>
              </View>
            </Block>
            <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          </Block>
        </ScrollView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  explore: {
    paddingHorizontal: theme.sizes.base * 1.2,
    paddingVertical: theme.sizes.base,
    height: height - (theme.sizes.base * 5)
  },
  category: {
    // this should be dynamic based on screen width
    minWidth: (width - (theme.sizes.padding * 2.0)),
    maxWidth: (width - (theme.sizes.padding * 2.0)),
    maxHeight: (width - (theme.sizes.padding * 2.4) - theme.sizes.base),
    marginVertical: theme.sizes.base
  },
  reviewAvatar: {
    height: theme.sizes.base * 3.15,
    width: theme.sizes.base * 3.15,
    borderRadius: (theme.sizes.base * 3.15) / 2,
    marginTop: theme.sizes.default
  },
  quote: {
    width: (width - (theme.sizes.base * 2.4)) * 0.80,
    paddingLeft: theme.sizes.small
  },
  header: {
    paddingHorizontal: theme.sizes.base * 1.2,
    height: theme.sizes.base * 5,
    backgroundColor: theme.colors.white,
    borderBottomWidth: 1,
    borderBottomColor: '#F1F1F1',
  },
  search: {
    height: theme.sizes.base * 2,
    width: width - theme.sizes.base * 2,
  },
  searchInput: {
    fontSize: theme.sizes.body,
    height: theme.sizes.base * 3,
    backgroundColor: 'rgba(142, 142, 147, 0.06)',
    borderColor: 'rgba(142, 142, 147, 0.06)',
    paddingLeft: theme.sizes.base / 1.333,
    paddingRight: theme.sizes.base * 1.5,
    marginLeft: theme.sizes.default
  },
  searchRight: {
    top: 0,
    marginVertical: theme.sizes.small,
    backgroundColor: 'transparent'
  },
  searchIcon: {
    position: 'absolute',
    right: theme.sizes.base / 1.333,
    top: theme.sizes.base / 1.6,
  },
});
