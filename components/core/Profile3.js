import React from 'react';
import { StyleSheet, Image, Dimensions, View, ScrollView } from 'react-native';
import { FontAwesome as Icon } from '@expo/vector-icons';

import { Button, Block, Text, Card, Divider } from '../../common';
import { theme } from '../../constants';

import MiniTag from './MiniTag';

const { width, height } = Dimensions.get('window');

export default class Profile extends React.Component {
  render() {
    return (
      <Block flex={false}>
        <ScrollView horizontal={true} contentContainerStyle={styles.explore} showsHorizontalScrollIndicator={false}>
          <Image
            source={require('../../assets/images/avatar.jpg')}
            style={styles.mainImage}
          />
          <Image
            source={require('../../assets/images/dj1.jpg')}
            style={styles.mainImage}
          />
        </ScrollView>
        <Block flex={false} style={{ paddingHorizontal: theme.sizes.base * 1.2, paddingTop: theme.sizes.padding - 5 }}>
          <Block flex={false} row space="between">
            <Block>
              <Text h3 bold>DJ Jessica</Text>
            </Block>
            <Block flex={false} row space="between" style={{ paddingLeft: theme.sizes.base * 3.5 }}>
              <Button shadow style={styles.headerCircle}>
                <Icon
                  name="heart"
                  size={14}
                  style={{ alignSelf: 'center' }}
                  color={theme.colors.primary}
                />
              </Button>
              <Button shadow style={styles.headerCircle}>
                <Icon
                  name="envelope"
                  size={14}
                  style={{ alignSelf: 'center' }}
                  color={theme.colors.primary}
                />
              </Button>
              <Button shadow style={styles.headerCircle}>
                <Icon
                  name="share"
                  size={14}
                  style={{ alignSelf: 'center' }}
                  color={theme.colors.primary}
                />
              </Button>
            </Block>
          </Block>
          <Block flex={false} row space="between" style={{ marginTop: theme.sizes.base }}>
            <Block row>
              <Icon
                name="map-marker"
                size={14}
                color={theme.colors.gray2}
                style= {styles.subIcon}
              />
              <Text body gray>Toronto</Text>
            </Block>
            <Block row>
              <Icon
                name="calendar"
                size={13}
                color={theme.colors.gray2}
                style= {{...styles.subIcon, marginTop: 1 }}
              />
              <Text body gray>December 21</Text>
            </Block>
            <Block row style={{ paddingLeft: theme.sizes.base }}>
              <Icon
                name="clock-o"
                size={16}
                color={theme.colors.gray2}
                style= {styles.subIcon}
              />
              <Text body gray>11pm - 2am</Text>
            </Block>
          </Block>
          <Block flex={false} style={styles.tags}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <MiniTag tag="Hip Hop" />
              <MiniTag tag="Afrobeats" />
              <MiniTag tag="EDM" />
              <MiniTag tag="Top 40" />
              <MiniTag tag="Disco" />
            </ScrollView>
          </Block>
          <Block flex={false} row space="between" style={{ marginTop: theme.sizes.base }}>
            <Card shadow style={{ marginRight: theme.sizes.small + 3 }}>
              <Text h2 bold>4.3</Text>
              <Block row>
                <Icon
                  name="star"
                  size={12}
                  color={theme.colors.primary}
                />
                <Icon
                  key={this.props.id}
                  name="star"
                  size={12}
                  stars
                  color={theme.colors.primary}
                />
                <Icon
                  key={this.props.id}
                  name="star"
                  size={12}
                  stars
                  color={theme.colors.primary}
                />
                <Icon
                  key={this.props.id}
                  name="star"
                  size={12}
                  stars
                  color={theme.colors.primary}
                />
              </Block>
            </Card>
            <Card shadow style={{ marginLeft: theme.sizes.small + 3 }}>
              <Block flex={false} row>
              <Icon
                name="music"
                size={24}
                style={{ marginRight: theme.sizes.default * 1.3, marginTop: theme.sizes.small, marginBottom: theme.sizes.default }}
                color={theme.colors.gray2}
              />
              <Icon
                name="image"
                size={24}
                style={{ marginTop: theme.sizes.small, marginBottom: theme.sizes.default }}
                color={theme.colors.gray2}
              />
              </Block>
              <Text body black bold>Samples</Text>
            </Card>
          </Block>
          <Button gradient onPress = {() => this.props.navigation.navigate('Booking')}>
            <Text center semibold white>Book @ $34/hr</Text>
          </Button>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  explore: {
    paddingHorizontal: theme.sizes.base * 1.2
  },
  mainImage: {
    width: (width - (theme.sizes.base * 3.5)),
    height: height / 3.75,
    resizeMode: 'cover',
    marginRight: theme.sizes.default,
    borderRadius: theme.sizes.radius,
    shadowColor: theme.colors.black,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 13
  },
  headerCircle: {
    height: theme.sizes.base * 1.75,
    width: theme.sizes.base * 1.75,
    borderRadius: (theme.sizes.base * 1.75) / 2,
    marginHorizontal: theme.sizes.small
  },
  subIcon: {
    marginRight: theme.sizes.small
  },
  tags: {
    marginTop: theme.sizes.base,
    marginRight: 0
  },
});
