import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React, { useState } from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import AppNavigator from './navigation/AppNavigator';

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false);

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <AppNavigator />
      </View>
    );
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require('./assets/images/avatar.jpg'),
      require('./assets/images/avatar2.jpg'),
      require('./assets/images/avatar3.jpg'),
      require('./assets/images/avatar4.jpg'),
      require('./assets/images/dj1.jpg'),
      require('./assets/images/dj2.jpg'),
      require('./assets/images/dj3.jpg'),
      require('./assets/images/intro.jpg'),
    ]),
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      ...Ionicons.font,
      // We include SpaceMono because we use it in HomeScreen.js. Feel free to
      // remove this if you are not using it in your app
      'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      'mahee-regular': require('./assets/fonts/MuktaMahee-Regular.ttf'),
      'mahee-medium': require('./assets/fonts/MuktaMahee-Medium.ttf'),
      'mahee-semibold': require('./assets/fonts/MuktaMahee-SemiBold.ttf'),
      'mahee-bold': require('./assets/fonts/MuktaMahee-Bold.ttf'),
      'mahee-extrabold': require('./assets/fonts/MuktaMahee-ExtraBold.ttf'),
      'nunito-light': require('./assets/fonts/NunitoSans-Light.ttf'),
      'nunito-regular': require('./assets/fonts/NunitoSans-Regular.ttf'),
    }),
  ]);
}

function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
