import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import MainTabNavigator from './MainTabNavigator';
import WelcomeScreen from '../screens/auth/WelcomeScreen';
import SignUpScreen from '../screens/auth/Signup';
import SignInScreen from '../screens/auth/Signin';
import ForgotScreen from '../screens/auth/Forgot';

const AuthStack = createStackNavigator({
  Welcome: WelcomeScreen,
  Signup: SignUpScreen,
  Login: SignInScreen,
  Forgot: ForgotScreen
});

export default createAppContainer(
  createSwitchNavigator({
    Auth: AuthStack,
    Main: MainTabNavigator
  })
);
