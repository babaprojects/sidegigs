import React from 'react';
import { Platform } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';

import { theme } from '../constants';

import TabBarIcon from '../components/nav/tabbar/TabBarIcon';
import TabBarIconMail from '../components/nav/tabbar/TabBarIconMail';
import TabBarIconHome from '../components/nav/tabbar/TabBarIconHome';
import TabBarIconCalendar from '../components/nav/tabbar/TabBarIconCalendar';

// Mains
import HomeScreen from '../screens/core/HomeScreen';
import DashScreen from '../screens/core/DashScreen';
import MessagesListScreen from '../screens/core/MessagesListScreen';
import ExploreScreen from '../screens/core/ExploreScreen';

// Secondary
import BookingScreen from '../screens/core/BookingScreen';
import ProfileScreen from '../screens/core/ProfileScreen';
import SettingsScreen from '../screens/core/SettingsScreen';
import WalletScreen from '../screens/core/WalletScreen';
import ChatScreen from '../screens/core/ChatScreen';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    Wallet: WalletScreen,
    Settings: SettingsScreen
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIconHome
      focused={focused}
      name={'home'}
    />
  ),
};

HomeStack.path = '';

const DashStack = createStackNavigator(
  {
    Dash: DashScreen,
  },
  config
);

DashStack.navigationOptions = {
  tabBarLabel: 'My Hub',
  tabBarIcon: ({ focused }) => (
    <TabBarIconCalendar
      focused={focused}
      name={'th-large'}
    />
  ),
};

DashStack.path = '';


const MessagesStack = createStackNavigator(
  {
    MessagesList: MessagesListScreen,
    Chat: ChatScreen
  },
  config
);

MessagesStack.navigationOptions = {
  tabBarLabel: 'Inbox',
  tabBarIcon: ({ focused }) => (
    <TabBarIconMail
      focused={focused}
      name={'envelope'}
    />
  ),
};

MessagesStack.path = '';


const ExploreStack = createStackNavigator(
  {
    Explore: ExploreScreen,
    Profile: ProfileScreen,
    Booking: BookingScreen,
  },
  config
);

ExploreStack.navigationOptions = {
  tabBarLabel: 'Explore',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={'list-ul'}
    />
  ),
};

ExploreStack.path = '';


const tabNavigator = createBottomTabNavigator({
  HomeStack,
  ExploreStack,
  DashStack,
}, {
  tabBarOptions: {
    activeTintColor: theme.colors.primary,
    inactiveTintColor: theme.colors.black,
    labelStyle: {
      fontSize: 12,
      fontFamily: 'mahee-semibold'
    },
    style: {
      height: 57,
      paddingVertical: theme.sizes.small,
      borderTopColor: '#F1F1F1',
      borderTopWidth: 1,
      backgroundColor: theme.colors.white,
      ...Platform.select({
        ios: {
          shadowColor: '#dbdbdb',
          shadowOffset: { height: -3 },
          shadowOpacity: 0.5,
          shadowRadius: 9,
        },
        android: {
          elevation: 20,
        },
      }),
    },
  },
});

tabNavigator.path = '';

export default tabNavigator;
