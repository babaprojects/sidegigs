const colors = {
  accent: "#F3534A",
  primary: "#D21F3C",
  secondary: "#D2321F",
  tertiary: "#FFE358",
  black: "#323643",
  white: "#FFFFFF",
  gray: "#9DA3B4",
  gray2: "#C5CCD6",
  gray3: "#E8E8E8",
  background: "#F1F1F1",
  tabIconDefault: "#ccc",
  tabIconSelected: "#0AC4BA",
  tabBar: "#fefefe",
  errorBackground: "red",
  errorText: "#fff",
  warningBackground: "#EAEB5E",
  warningText: "#666804",
  noticeBackground: "#0AC4BA",
  facebookColor: "#3b5998"
};

const sizes = {
  // global sizes
  base: 16,
  font: 14,
  radius: 15,
  radiusB: 25,
  padding: 25,
  small: 5,
  default: 10,

  // font sizes
  h1: 32,
  h2: 26,
  h3: 22,
  h4: 20,
  title: 18,
  header: 16,
  body: 13,
  caption: 12,
  small1: 10,
  smallx: 8.5,
};

const fonts = {
  h1: {
    fontSize: sizes.h1
  },
  h2: {
    fontSize: sizes.h2
  },
  h3: {
    fontSize: sizes.h3
  },
  h4: {
    fontSize: sizes.h4
  },
  header: {
    fontSize: sizes.header
  },
  title: {
    fontSize: sizes.title
  },
  body: {
    fontSize: sizes.body
  },
  caption: {
    fontSize: sizes.caption
  },
  small1: {
    fontSize: sizes.small1
  },
  smallx: {
    fontSize: sizes.smallx
  },
};

export { colors, sizes, fonts };
