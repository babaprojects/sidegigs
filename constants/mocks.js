
const profile = {
  username: 'djjessica',
  location: 'Toronto',
  email: 'user@test.com',
  avatar: require('../assets/images/avatar.jpg'),
  budget: 1000,
  monthly_cap: 5000,
  notifications: true,
  newsletter: false,
};

export {
  profile,
}
