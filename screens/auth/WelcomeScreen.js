import React from 'react';
import { StyleSheet, Image, Dimensions } from 'react-native';

import { Button, Block, Text } from '../../common';
import { theme } from '../../constants';

const { width, height } = Dimensions.get('window');

export default class WelcomeScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <Block>
        <Block center middle flex={0.4} marginTop={35}>
          <Text h1 center bold>Welcome to Sidegiggs</Text>
          <Text h3 gray2 style={{ marginVertical: theme.sizes.padding / 2 }}>Book the best artists here</Text>
        </Block>
        <Block center middle margin={0}>
          <Image
            source={require('../../assets/images/intro.jpg')}
            resizeMode="contain"
            style={{ width: width, height: height / 2, overflow: 'visible' }}
          />
        </Block>
        <Block middle flex={0.5} margin={[0, theme.sizes.padding * 2]}>
          <Button gradient onPress = {() => this.props.navigation.navigate('Login')}>
            <Text center semibold white>Login</Text>
          </Button>
          <Button shadow onPress = {() => this.props.navigation.navigate('Signup')}>
            <Text center semibold>Create An Account</Text>
          </Button>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gray',
    justifyContent: 'flex-end'
  },
});
