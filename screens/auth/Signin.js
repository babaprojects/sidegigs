import React from 'react';
import { StyleSheet, ActivityIndicator, Keyboard, KeyboardAvoidingView } from 'react-native';
import { Ionicons as Icon} from '@expo/vector-icons';

import { Button, Block, Input, Text } from '../../common';
import { theme } from '../../constants';

const VALID_EMAIL = "user@test.com";
const VALID_PASSWORD = "welcome";

export default class SignInScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {
    email: '',
    password: '',
    errors: [],
    loading: false,
  }

  handleLogin() {
    const { navigation } = this.props;
    const { email, password } = this.state;
    const errors = [];

    Keyboard.dismiss();
    this.setState({ loading: true });

    // check with backend API or with some static data
    if (email !== VALID_EMAIL) {
      errors.push('email');
    }
    if (password !== VALID_PASSWORD) {
      errors.push('password');
    }

    this.setState({ errors, loading: false });

    if (!errors.length) {
      navigation.navigate("Main");
    }
  }

  render() {
    const { navigation } = this.props;
    const { loading, errors } = this.state;
    const hasErrors = key => errors.includes(key) ? styles.hasErrors : null;

    return (
      <KeyboardAvoidingView style={styles.login} behavior="padding">
        <Block padding={[0, theme.sizes.base * 2]}>
          <Block middle>
            <Block flex={false} row center space="between" style={styles.header}>
              <Button onPress={() => navigation.goBack()}>
                <Icon
                  name="ios-arrow-round-back"
                  size={39}
                  style={styles.backButton}
                  color={theme.colors.primary}
                />
              </Button>
              <Text h1 primary bold>Login.</Text>
            </Block>
            <Input
              label="Email"
              error={hasErrors('email')}
              style={[styles.input, hasErrors('email')]}
              defaultValue={this.state.email}
              onChangeText={text => this.setState({ email: text })}
            />
            <Input
              secure
              label="Password"
              error={hasErrors('password')}
              style={[styles.input, hasErrors('password')]}
              defaultValue={this.state.password}
              onChangeText={text => this.setState({ password: text })}
            />
            <Button gradient onPress={() => this.handleLogin()}>
              {loading ?
                <ActivityIndicator size="small" color="white" /> :
                <Text bold white center>Login</Text>
              }
            </Button>
            <Button shadow style={{ backgroundColor: theme.colors.facebookColor, marginVertical: 15 }} onPress = {() => null}>
              <Text center semibold white>Login With Facebook</Text>
            </Button>
            <Button onPress={() => navigation.navigate('Forgot')} style={{ marginTop: 55 }}>
              <Text gray center style={{ textDecorationLine: 'underline' }}>
                Forgot your password?
              </Text>
            </Button>

          </Block>
        </Block>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  login: {
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    marginBottom: theme.sizes.padding * 2,
  },
  backButton: {
    marginTop: theme.sizes.small,
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  hasErrors: {
    borderBottomColor: theme.colors.accent,
  }
});
