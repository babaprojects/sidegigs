import React, { Component } from 'react';
import { Alert, ActivityIndicator, Keyboard, KeyboardAvoidingView, StyleSheet } from 'react-native';
import { Ionicons as Icon} from '@expo/vector-icons';

import { Button, Block, Input, Text } from '../../common';
import { theme } from '../../constants';

const VALID_EMAIL = "user@test.com";

export default class ForgotScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {
    email: '',
    errors: [],
    loading: false,
  }

  handleForgot() {
    const { navigation } = this.props;
    const { email } = this.state;
    const errors = [];

    Keyboard.dismiss();
    this.setState({ loading: true });

    // check with backend API or with some static data
    if (email !== VALID_EMAIL) {
      errors.push('email');
    }

    this.setState({ errors, loading: false });

    if (!errors.length) {
      Alert.alert(
        'Password sent!',
        'Please check you email.',
        [
          {
            text: 'OK', onPress: () => {
              navigation.navigate('Login')
            }
          }
        ],
        { cancelable: false }
      )
    } else {
      Alert.alert(
        'Error',
        'Please enter the correct email.',
        [
          { text: 'Try again', }
        ],
        { cancelable: false }
      )
    }
  }

  render() {
    const { navigation } = this.props;
    const { loading, errors } = this.state;
    const hasErrors = key => errors.includes(key) ? styles.hasErrors : null;

    return (
      <KeyboardAvoidingView style={styles.forgot} behavior="padding">
        <Block padding={[0, theme.sizes.base * 2]}>
          <Block middle style={{ marginBottom: theme.sizes.padding * 5 }}>
            <Block flex={false} row center space="between" style={styles.header}>
              <Button onPress={() => navigation.goBack()}>
                <Icon
                  name="ios-arrow-round-back"
                  size={39}
                  style={styles.backButton}
                  color={theme.colors.primary}
                />
              </Button>
              <Text h1 primary bold>Reset Password.</Text>
            </Block>
            <Input
              label="Email"
              error={hasErrors('email')}
              style={[styles.input, hasErrors('email')]}
              defaultValue={this.state.email}
              onChangeText={text => this.setState({ email: text })}
            />
            <Button gradient onPress={() => this.handleForgot()}>
              {loading ?
                <ActivityIndicator size="small" color="white" /> :
                <Text bold white center>Reset Password</Text>
              }
            </Button>
          </Block>
        </Block>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  forgot: {
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    marginBottom: theme.sizes.padding * 5,
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  hasErrors: {
    borderBottomColor: theme.colors.accent,
  }
})
