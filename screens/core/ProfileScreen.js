import React from 'react';
import { StyleSheet, ActivityIndicator, Keyboard } from 'react-native';
import { Ionicons as Icon} from '@expo/vector-icons';

import { Button, Block, Input, Text } from '../../common';
import { theme } from '../../constants';

import ProfileHeader from '../../components/nav/headers/ProfileHeader';
import Profile from '../../components/core/Profile';

export default class ProfileScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: <ProfileHeader navigation={navigation} />
    };
  };

  render() {

    return (
      <Profile />
    );
  }
}

const styles = StyleSheet.create({
  backButton: {
    marginLeft: theme.sizes.padding - 3,
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  hasErrors: {
    borderBottomColor: theme.colors.accent,
  }
});
