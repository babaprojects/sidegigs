import React from 'react';
import { StyleSheet, Image, Dimensions, ScrollView } from 'react-native';

import { Button, Block, Text } from '../../common';
import { theme } from '../../constants';

import MessagesList from '../../components/core/MessagesList';

const { width, height } = Dimensions.get('window');

export default class ChatScreen extends React.Component {
  static navigationOptions = {
    title: 'Chat'
  };

  state = {
    messages: [],
  }

  componentWillMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: 'Hello DJ this is nice',
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://placeimg.com/140/140/any',
          },
        },
      ],
    })
  }

  onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
  }

  render() {
    return (
      <Block animated>
        <Text>Hello</Text>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  explore: {
    paddingHorizontal: theme.sizes.base * 1.6
  },
});
