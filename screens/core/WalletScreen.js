import React from 'react';
import { StyleSheet, Image, Dimensions, ScrollView, View, Modal, Animated } from 'react-native';
import { FontAwesome as Icon} from '@expo/vector-icons';

import { Button, Block, Text, Card, Divider } from '../../common';
import { theme } from '../../constants';

import Profile from '../../components/core/Profile';
import GigsterCard from '../../components/core/GigsterCard';
import MainHeader from '../../components/nav/headers/MainHeader';

const { width, height } = Dimensions.get('window');

export default class WalletScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: <MainHeader navigation={navigation} />
    };
  };


  render() {
    return (
      <Block>
        <View style={styles.stats}>
          <Block row space="between">
            <Block style={{ borderRightColor: theme.colors.background, borderRightWidth: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Text bold style={{ fontSize: 34 }}>$155.20</Text>
              <Text body gray transform="uppercase">Amount Earned</Text>
            </Block>
            <Block style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text bold style={{ fontSize: 34 }}>$2000.20</Text>
            <Text body gray transform="uppercase">Amount Spent</Text>
            </Block>
          </Block>
        </View>
        <Block style={styles.explore}>
          <Button shadow style={styles.walletButton}>
            <Block row>
              <Icon
                name="money"
                size={18}
                color={theme.colors.primary}
                style={styles.walletIcon}
              />
              <Text gray header semibold>Withdraw Funds</Text>
            </Block>
          </Button>
          <Button shadow style={styles.walletButton}>
            <Block row>
              <Icon
                name="credit-card-alt"
                size={15}
                color={theme.colors.primary}
                style={styles.walletIcon}
              />
              <Text gray header semibold>Add A New Card</Text>
            </Block>
          </Button>
          <Button shadow style={styles.walletButton}>
            <Block row>
              <Icon
                name="warning"
                size={18}
                color={theme.colors.primary}
                style={styles.walletIcon}
              />
              <Text gray header semibold>Report Gig</Text>
            </Block>
          </Button>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  explore: {
    paddingHorizontal: theme.sizes.base * 1.8,
    paddingTop: theme.sizes.base,
    paddingBottom: theme.sizes.base * 2
  },
  stats: {
    height: theme.sizes.base * 7,
    marginTop: theme.sizes.base * 4,
    marginBottom: theme.sizes.base * 2
  },
  walletButton: {
    paddingLeft: theme.sizes.base * 1.5,
    paddingTop: theme.sizes.default
  },
  walletIcon: {
    marginTop: theme.sizes.small,
    marginRight: theme.sizes.default
  }
});
