import React from 'react';
import { StyleSheet, Image, Dimensions, View, Platform, ScrollView, Animated, TouchableOpacity } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import { FontAwesome as Stars } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

import { Button, Block, Input, Text, Card, Divider } from '../../common';
import { theme } from '../../constants';

import Tag from '../../components/core/Tag';
import GigsterCard from '../../components/core/GigsterCard';
import MainHeader from '../../components/nav/headers/MainHeader';

const { width, height } = Dimensions.get('window');

export default class ExploreScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: <MainHeader navigation={navigation} />
    };
  };

  render() {
    return (
      <Block animated background>
        <Block flex={false} style={styles.header}>
          <ScrollView horizontal={true} style={styles.tag} scrollEventThrottle={16} showsHorizontalScrollIndicator={false}>
            <Tag
              tag="EDM"
            />
            <Tag
              tag="Hip Hop"
            />
            <Tag
              tag="Top 40"
            />
            <Tag
              tag="Jazz"
            />
            <Tag
              tag="Freestyle"
            />
            <Tag
              tag="Kids"
            />
          </ScrollView>
        </Block>
        <Animated.ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.explore}>
          <GigsterCard
            id={1}
            avatar={require('../../assets/images/avatar3.jpg')}
            name="DJ Freddy X"
            location="TORONTO"
            distance="12.5 km"
            gigs="10"
            rate="34"
            rating={2}
            navigation={this.props.navigation}
          />
          <GigsterCard
            id={2}
            avatar={require('../../assets/images/avatar4.jpg')}
            name="AvaAva"
            location="TORONTO"
            distance="1.9 km"
            gigs="2"
            rate="75"
            rating={5}
            navigation={this.props.navigation}
          />
          <GigsterCard
            id={3}
            avatar={require('../../assets/images/avatar2.jpg')}
            name="Jessica Sanchez"
            location="TORONTO"
            distance="34.0 km"
            gigs="58"
            rate="300"
            rating={4}
            navigation={this.props.navigation}
          />
        </Animated.ScrollView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  explore: {
    paddingHorizontal: theme.sizes.base * 1.2,
    paddingVertical: theme.sizes.padding
  },
  header: {
    paddingHorizontal: theme.sizes.base * 1.2,
    paddingBottom: theme.sizes.base,
    height: theme.sizes.base * 5,
    backgroundColor: theme.colors.white,
    borderBottomWidth: 1,
    borderBottomColor: '#F1F1F1',
    ...Platform.select({
      ios: {
        shadowColor: '#dbdbdb',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.5,
        shadowRadius: 9,
      },
      android: {
        elevation: 20,
      },
    }),
  },
  tags: {
    backgroundColor: 'transparent',
    marginRight: 0,
  },
})
