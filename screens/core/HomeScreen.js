import React from 'react';
import { StyleSheet, Image, Dimensions, ScrollView, View, Modal, Animated } from 'react-native';
import { Ionicons as Icon} from '@expo/vector-icons';

import { Button, Block, Text, Card, Divider } from '../../common';
import { theme } from '../../constants';

import Profile from '../../components/core/Profile';
import GigsterCard from '../../components/core/GigsterCard';
import MainHeader from '../../components/nav/headers/MainHeader';

const { width, height } = Dimensions.get('window');

export default class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: <MainHeader navigation={navigation} />
    };
  };


  render() {
    return (
      <Block>
        <Animated.ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.explore}>
          <Block>
            <Block style={{ marginVertical: theme.sizes.base }} row>
              <Divider />
              <Text h3 bold style={{ margin: theme.sizes.base }}>The Spotlight</Text>
              <Divider />
            </Block>
            <Image
              source={require('../../assets/images/dj1.jpg')}
              style={styles.mainImage}
            />
            <Block row>
              <Image
                source={require('../../assets/images/dj2.jpg')}
                style={styles.miniImage}
              />
              <Image
                source={require('../../assets/images/dj3.jpg')}
                style={{ ...styles.miniImage, marginLeft: theme.sizes.default }}
              />
            </Block>
          </Block>

          <Block>
            <Block style={{ marginVertical: theme.sizes.base }} row>
              <Divider />
              <Text h3 bold style={{ margin: theme.sizes.base }}>Featured Gigs</Text>
              <Divider />
            </Block>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <Image
                source={require('../../assets/images/dj2.jpg')}
                style={styles.featuredImage}
              />
              <Image
                source={require('../../assets/images/dj3.jpg')}
                style={{ ...styles.featuredImage, marginLeft: theme.sizes.default }}
              />
              <Image
                source={require('../../assets/images/dj1.jpg')}
                style={{ ...styles.featuredImage, marginLeft: theme.sizes.default }}
              />
              <Image
                source={require('../../assets/images/avatar2.jpg')}
                style={{ ...styles.featuredImage, marginLeft: theme.sizes.default }}
              />
              <Image
                source={require('../../assets/images/avatar3.jpg')}
                style={{ ...styles.featuredImage, marginLeft: theme.sizes.default }}
              />
            </ScrollView>
          </Block>

          <Block>
            <Block style={{ marginVertical: theme.sizes.base }} row>
              <Divider />
              <Text h3 bold style={{ margin: theme.sizes.base }}>Top Gigsters</Text>
              <Divider />
            </Block>
            <GigsterCard
              id={1}
              avatar={require('../../assets/images/avatar3.jpg')}
              name="DJ Freddy X"
              location="TORONTO"
              distance="12.5 km"
              gigs="10"
              rate="34"
              rating={2}
              navigation={this.props.navigation}
            />
            <GigsterCard
              id={2}
              avatar={require('../../assets/images/avatar4.jpg')}
              name="AvaAva"
              location="TORONTO"
              distance="1.9 km"
              gigs="2"
              rate="75"
              rating={5}
              navigation={this.props.navigation}
            />
            <GigsterCard
              id={3}
              avatar={require('../../assets/images/avatar2.jpg')}
              name="Jessica Sanchez"
              location="TORONTO"
              distance="34.0 km"
              gigs="58"
              rate="300"
              rating={4}
              navigation={this.props.navigation}
            />
          </Block>
        </Animated.ScrollView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  explore: {
    paddingHorizontal: theme.sizes.base * 1.2,
    paddingBottom: theme.sizes.base * 2
  },
  mainImage: {
    width: (width - (theme.sizes.base * 2.4)),
    height: (width - (theme.sizes.base * 2.4)) / 2,
    resizeMode: 'cover',
    borderRadius: theme.sizes.radius,
    shadowColor: theme.colors.black,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 13
  },
  featuredImage: {
    width: (width - (theme.sizes.base * 2.4 + 10)) / 3,
    height: (width - (theme.sizes.base * 2.4 + 10)) / 3,
    marginTop: theme.sizes.default,
    borderRadius: theme.sizes.radius,
    shadowColor: theme.colors.black,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 13
  },
  miniImage: {
    width: (width - (theme.sizes.base * 2.4 + 10)) / 2,
    height: (width - (theme.sizes.base * 2.4 + 10)) / 2,
    marginTop: theme.sizes.default,
    borderRadius: theme.sizes.radius,
    shadowColor: theme.colors.black,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 13
  }
});
