import React from 'react';
import { StyleSheet, Image, Dimensions, ScrollView, Modal, Animated } from 'react-native';

import { FontAwesome as Icon } from '@expo/vector-icons';

import { Button, Block, Text, Card, Divider, Input } from '../../common';
import { theme } from '../../constants';

import Bookings from '../../components/core/Bookings';
import MessagesList from '../../components/core/MessagesList';
import Settings from '../../components/core/Settings';
import Profile from '../../components/core/Profile';
import Wallet from '../../components/core/Wallet';
import MainHeader from '../../components/nav/headers/MainHeader';

const { width, height } = Dimensions.get('window');

export default class DashScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: <MainHeader navigation={navigation} />
    };
  };

  state = {
    showSettings: false,
    showSupport: false,
    showWallet: false,
    showProfile: false,
    showBookings: false,
    showMessages: false
  }

  renderBookings() {
    return (
      <Modal animationType="slide" visible={this.state.showBookings} onRequestClose={() => this.setState({ showBookings: false })}>
        <Block style={{ paddingTop: theme.sizes.padding * 1.5 }}>
          <Icon
            name="close"
            size={21}
            style={styles.closeButton}
            color={theme.colors.gray}
            onPress={() => this.setState({ showBookings: false })}
          />
          <Block flex={false} style={{ paddingTop: theme.sizes.padding * 1.5, paddingHorizontal: theme.sizes.base * 1.2 }}>
            <Bookings />
          </Block>
        </Block>
      </Modal>
    )
  }

  renderProfile() {
    return (
      <Modal animationType="slide" visible={this.state.showProfile} onRequestClose={() => this.setState({ showProfile: false })}>
        <Block style={{ paddingTop: theme.sizes.padding * 1.5 }}>
          <Icon
            name="close"
            size={21}
            style={styles.closeButton}
            color={theme.colors.gray}
            onPress={() => this.setState({ showProfile: false })}
          />
          <Block flex={false} style={{ paddingTop: theme.sizes.padding }}>
            <Text semibold center title style={{ marginBottom: theme.sizes.padding }}>This is a preview of your profile</Text>
            <Profile />
          </Block>
        </Block>
      </Modal>
    )
  }

  renderMessages() {
    return (
      <Modal animationType="slide" visible={this.state.showMessages} onRequestClose={() => this.setState({ showMessages: false })}>
        <Block style={{ paddingTop: theme.sizes.base + 3 }}>
          <Icon
            name="close"
            size={21}
            style={styles.closeButton}
            color={theme.colors.gray}
            onPress={() => this.setState({ showMessages: false })}
          />
          <Block flex={false}>
            <MessagesList />
          </Block>
        </Block>
      </Modal>
    )
  }

  renderWallet() {
    return (
      <Modal animationType="slide" visible={this.state.showWallet} onRequestClose={() => this.setState({ showWallet: false })}>
        <Block style={{ paddingTop: theme.sizes.padding * 1.5 }}>
          <Icon
            name="close"
            size={21}
            style={styles.closeButton}
            color={theme.colors.gray}
            onPress={() => this.setState({ showWallet: false })}
          />
          <Block flex={false} style={{ paddingTop: theme.sizes.padding * 1.5, paddingHorizontal: theme.sizes.base * 1.2 }}>
            <Wallet />
          </Block>
        </Block>
      </Modal>
    )
  }

  renderSupport() {
    return (
      <Modal animationType="slide" visible={this.state.showSupport} onRequestClose={() => this.setState({ showSupport: false })}>
        <Block style={{ paddingTop: theme.sizes.padding * 1.5 }}>
          <Icon
            name="close"
            size={21}
            style={styles.closeButton}
            color={theme.colors.gray}
            onPress={() => this.setState({ showSupport: false })}
          />
          <Block flex={false} style={{ paddingTop: theme.sizes.padding * 2, paddingHorizontal: theme.sizes.base * 1.2 }}>
            <Text bold h1 black>Submit A Ticket</Text>
            <Input
              placeholder="Title"
              placeholderTextColor={theme.colors.black}
              style={[styles.input]}
              onChangeText={text => this.setState({ email: text })}
            />

            <Input
              placeholder="Message"
              multiline={true}
              numberOfLines={10}
              placeholderTextColor={theme.colors.black}
              style={[styles.input, styles.description]}
              onChangeText={text => this.setState({ email: text })}
            />
            <Button gradient onPress={() => this.setState({ showSupport: false })}>
              <Text center semibold white>Submit Ticket</Text>
            </Button>
          </Block>
        </Block>
      </Modal>
    )
  }

  renderSettings() {
    return (
      <Modal animationType="slide" visible={this.state.showSettings} onRequestClose={() => this.setState({ showSettings: false })}>
        <Block style={{ paddingTop: theme.sizes.padding * 1.5 }}>
          <Icon
            name="close"
            size={21}
            style={styles.closeButton}
            color={theme.colors.gray}
            onPress={() => this.setState({ showSettings: false })}
          />
          <ScrollView contentContainerStyle={{ paddingBottom: theme.sizes.padding }}>
            <Settings />
          </ScrollView>
        </Block>
      </Modal>
    )
  }

  render() {
    return (
      <Block animated background flex={false}>
        <Block flex={false} center style={styles.modal}>
          <Text bold primary style={{ fontSize: 48, marginBottom: 0 }}>$155.20</Text>
          <Text bold body gray transform="uppercase" style={{ marginTop: -10 }}>Amount Earned</Text>
        </Block>
        <ScrollView horizontal={true} contentContainerStyle={styles.explore} showsHorizontalScrollIndicator={false}>
          <Card shadow style={styles.card}>
            <Block row flex={false} space="between">
              <Block style={{ paddingTop: theme.sizes.default }}>
                <Icon
                  name="calendar"
                  size={28}
                  color={theme.colors.gray2}
                  style={{...styles.subIcon, marginBottom: theme.sizes.small + 2 }}
                />
                <Text bold gray caption>Bookings</Text>
              </Block>
              <Block row right>
                <Text bold right h2>2</Text>
              </Block>
            </Block>
          </Card>
          <Card shadow style={styles.card}>
            <Block row flex={false} space="between">
              <Block style={{ paddingTop: theme.sizes.default }}>
                <Icon
                  name="money"
                  size={35}
                  color={theme.colors.gray2}
                  style={styles.subIcon}
                />
                <Text bold gray caption>Wallet</Text>
              </Block>
              <Block row right>
                <Text bold right h2>$111.25</Text>
              </Block>
            </Block>
          </Card>
          <Card shadow style={styles.card}>
            <Block row flex={false} space="between">
              <Block style={{ paddingTop: theme.sizes.default }}>
                <Icon
                  name="briefcase"
                  size={35}
                  color={theme.colors.gray2}
                  style={styles.subIcon}
                />
                <Text bold gray caption>Total Gigs</Text>
              </Block>
              <Block row right>
                <Text bold right h2>15</Text>
              </Block>
            </Block>
          </Card>
          <Card shadow style={styles.card}>
            <Block row space="between">
              <Block style={{ paddingTop: theme.sizes.default }}>
                <Icon
                  name="star"
                  size={35}
                  color={theme.colors.gray2}
                />
                <Text bold gray caption>Avg. Rating</Text>
              </Block>
              <Block row right>
                <Text bold right h2>4.3</Text>
              </Block>
            </Block>
          </Card>
        </ScrollView>
        <Block flex={false} padding={theme.sizes.base}>
          <Block flex={false} row>
            <Divider />
            <Text h3 bold style={{ margin: theme.sizes.base }}>GO TO</Text>
            <Divider />
          </Block>
          <Block flex={false} row center space="between" style={{ paddingHorizontal: theme.sizes.padding * 1.6 }}>
            <Button shadow style={styles.headerCircle} onPress={() => this.setState({ showBookings: true })}>
              <Icon
                name="calendar"
                size={17}
                style={{ alignSelf: 'center' }}
                color={theme.colors.white}
              />
              <Text center smallx white semibold>Bookings</Text>
            </Button>
            <Button shadow style={styles.headerCircle} onPress={() => this.setState({ showProfile: true })}>
              <Icon
                name="user"
                size={18}
                style={{ alignSelf: 'center' }}
                color={theme.colors.white}
              />
              <Text center smallx white semibold>Profile</Text>
            </Button>
            <Button
              shadow
              style={styles.headerCircle}
              onPress={() => this.setState({ showMessages: true })}
            >
              <Icon
                name="envelope"
                size={18}
                style={{ alignSelf: 'center' }}
                color={theme.colors.white}
              />
              <Text center smallx white semibold>Inbox</Text>
            </Button>
          </Block>
          <Block flex={false} row center space="between" style={{ paddingHorizontal: theme.sizes.padding * 1.6 }}>
            <Button shadow style={styles.headerCircle} onPress={() => this.setState({ showWallet: true })}>
              <Icon
                name="money"
                size={22}
                style={{ alignSelf: 'center' }}
                color={theme.colors.white}
              />
              <Text center smallx white semibold>Wallet</Text>
            </Button>
            <Button shadow style={styles.headerCircle} onPress={() => this.setState({ showSupport: true })}>
              <Icon
                name="question"
                size={20}
                style={{ alignSelf: 'center' }}
                color={theme.colors.white}
              />
              <Text center smallx white semibold>Support</Text>
            </Button>
            <Button
              shadow
              style={styles.headerCircle}
              onPress={() => this.setState({ showSettings: true })}
            >
              <Icon
                name="cog"
                size={20}
                style={{ alignSelf: 'center' }}
                color={theme.colors.white}
              />
              <Text center smallx white semibold>Settings</Text>
            </Button>
          </Block>
          {this.renderBookings()}
          {this.renderWallet()}
          {this.renderSupport()}
          {this.renderSettings()}
          {this.renderProfile()}
          {this.renderMessages()}
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  explore: {
    paddingHorizontal: theme.sizes.base * 1.2,
    paddingTop: theme.sizes.padding
  },
  headerCircle: {
    height: theme.sizes.base * 3.95,
    width: theme.sizes.base * 3.95,
    borderRadius: (theme.sizes.base * 3.95) / 2,
    backgroundColor: theme.colors.primary
  },
  card: {
    marginRight: theme.sizes.base,
    height: theme.sizes.padding * 4.5,
    width: width /2
  },
  closeButton: {
    alignSelf: 'flex-end',
    marginRight: theme.sizes.padding * 1.25
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginBottom: theme.sizes.default
  },
  description: {
    height: height / 4
  }
});
