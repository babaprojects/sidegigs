import React from 'react';
import { StyleSheet, Image, Dimensions } from 'react-native';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import { Feather as Icon } from '@expo/vector-icons';

import { Button, Block, Card, Text, Divider } from '../../common';
import { theme } from '../../constants';

const { width, height } = Dimensions.get('window');

export default class BookingScreen extends React.Component {
  static navigationOptions = {
    title: 'Booking'
  };

  render() {
    return (
      <Block style={styles.container}>
        <Card shadow>
          <CalendarList
            // Initially visible month. Default = Date()
            current={'2019-11-23'}
            // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
            minDate={'2019-11-01'}
            // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
            maxDate={'2050-05-30'}
            // Handler which gets executed when visible month changes in calendar. Default = undefined
            renderArrow={(direction) => (<Icon
              name="arrow-right"
              size={24}
              style={{ marginRight: theme.sizes.small }}
            />)}
            // Do not show days of other months in month page. Default = false
            hideExtraDays={true}
            hideArrows={true}
            horizontal={true}
            // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
            // day from another month that is visible in calendar page. Default = false
            disableMonthChange={true}
            // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
            firstDay={1}
            // Hide day names. Default = false
            hideDayNames={true}
            // Show week numbers to the left. Default = false
            showWeekNumbers={true}
            calendarWidth={320}
            calendarHeight={height / 2.05}
            theme={{
              backgroundColor: '#ffffff',
              calendarBackground: '#ffffff',
              textSectionTitleColor: '#b6c1cd',
              selectedDayBackgroundColor: theme.colors.primary,
              selectedDayTextColor: '#ffffff',
              todayTextColor: '#00adf5',
              dayTextColor: '#2d4150',
              textDisabledColor: '#d9e1e8',
              dotColor: '#00adf5',
              selectedDotColor: '#ffffff',
              monthTextColor: theme.colors.primary,
              indicatorColor: theme.colors.primary,
            }}
          />
          <Divider margin={[theme.sizes.small, theme.sizes.default]} />
            <Text gray h2 center>2:00pm</Text>
          <Divider margin={[theme.sizes.small, theme.sizes.default]} />
          <Button gradient onPress = {() => this.props.navigation.navigate('Booking')}>
            <Text center semibold white>$150.50</Text>
          </Button>
        </Card>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: theme.sizes.base * 1.6,
    marginVertical: theme.sizes.small
  },
});
